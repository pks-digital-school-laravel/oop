<?php
    require_once("animal.php");
    require_once("frog.php");
    require_once("ape.php");
        
    $sheep = new Animal("shaun");

    //release 0
    echo "<strong>Release 0 </strong><br><br>";

    echo "nama : ".$sheep->name."<br>"; // "shaun"
    echo "jumlah kaki :".$sheep->legs."<br>"; // 4
    echo "apakah termasuk hewan berdarah dingin : ".$sheep->cold_blooded."<br><br>"; // "no"

    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
    echo "Menggunakan method get (get_name(), get_legs(), get_cold_blooded())<br>";
    echo "nama : ".$sheep->get_name()."<br>";
    echo "jumlah kaki :".$sheep->get_legs()."<br>";
    echo "apakah termasuk hewan berdarah dingin : ".$sheep->get_cold_blooded()."<br></br>";

    //release 1
    echo "------------------------------------<br>";
    echo "<strong>Release 1 </strong><br><br>";
    
    echo "Name : ".$sheep->get_name()."<br>";
    echo "legs : ".$sheep->get_legs()."<br>";
    echo "cold blooded : ".$sheep->get_cold_blooded()."<br><br>";

    $kodok = new Frog("buduk");
    echo "Name : ".$kodok->get_name()."<br>";
    echo "legs : ".$kodok->get_legs()."<br>";
    echo "cold blooded : ".$kodok->get_cold_blooded()."<br>";
    echo "Jump : ".$kodok->jump()."<br><br>";

    $sungokong = new Ape("kera sakti");
    echo "Name : ".$sungokong->get_name()."<br>";
    echo "legs : ".$sungokong->get_legs()."<br>";
    echo "cold blooded : ".$sungokong->get_cold_blooded()."<br>";
    echo "Yell : ".$sungokong->yell()."<br><br>";