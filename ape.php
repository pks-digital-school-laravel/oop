<?php

    require_once("animal.php");
    
    class Ape extends Animal {
        function __construct($name)
        {
            $this->name = $name;
            $this->legs = 2;
        }
        function yell(){
            return "Auooo";
        }  
    }